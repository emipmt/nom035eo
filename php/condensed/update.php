<?php

  require_once '../conn.php';
  require_once 'read.php';

  $id = $_POST['id'];
  $title = $_POST['title'];
  $aspects = $_POST['aspects'];
  $range = json_decode($_POST["range"], true);

  $sqlUpdateCondensed = "UPDATE condensed SET title = '{$title}', aspects = '{$aspects}' WHERE id = '{$id}' ";
  if (mysqli_query($conn, $sqlUpdateCondensed)) {
	$response;

	for($i = 0; $i < count($range); $i++){
		$sql = "UPDATE condensedranges SET min = {$range[$i]["min"]}, max = {$range[$i]["max"]} WHERE idcondensed = '{$id}' AND rangepos = {$range[$i]["rangepos"]}";

		if(mysqli_query($conn, $sql)){
			$response->status = true;
			$response->message = 'Condensado actualizado correctamente';
			$response->condensed = readCondensed($sqlReadCondensed,$conn);
		} else {
			$response->status = false;
    		$response->message = 'Algo salió mal, intentalo nuevamente más tarde';
		}

	}
    echo json_encode($response);
  } else {
    $response->status = false;
    $response->message = 'Algo salió mal, intentalo nuevamente más tarde';
    echo json_encode($response);
  }
