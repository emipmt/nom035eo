<?php

  require_once '../conn.php';
  require_once 'read.php';

  $title = $_POST['title'];
  $aspects = $_POST['aspects'];
  $rangeMin = explode("," , $_POST['rangeMin']);
  $rangeMax = explode("," , $_POST['rangeMax']);

  $sqlCreateCondensed = "INSERT INTO condensed (title,aspects) VALUES ('{$title}','{$aspects}')";
  if (mysqli_query($conn, $sqlCreateCondensed)) {


	$idcondensed = $conn->insert_id;

	$values = "";
	for($i = 0; $i < count($rangeMin); $i++){
		if($i == count($rangeMin) - 1){
			$values = $values . "({$idcondensed}, {$rangeMin[$i]}, {$rangeMax[$i]}, {$i}) ";
		} else {
			$values = $values . "({$idcondensed}, {$rangeMin[$i]}, {$rangeMax[$i]}, {$i}), ";
		}
	}

	$sql = "INSERT INTO condensedranges (idcondensed, min, max, rangepos) VALUES " . $values;

	if(mysqli_query($conn, $sql)){
		$response->status = true;
		$response->message = 'Condensado creado correctamente';
		$response->condensed = readCondensed($sqlReadCondensed,$conn);
		echo json_encode($response);
	} else {
		$response->error = mysqli_error($conn);
		$response->status = false;
		$response->message = 'Algo salió mal, intentalo nuevamente más tarde';
		echo json_encode($response);
	}
  } else {
    $response->status = false;
    $response->message = 'Algo salió mal, intentalo nuevamente más tarde';
    echo json_encode($response);
  }
